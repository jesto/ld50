using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    
    void Start()
    {
	    rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
	    rb.velocity = new Vector2(
		    Time.deltaTime * Input.GetAxis("Horizontal") * speed, 
		    Time.deltaTime * Input.GetAxis("Vertical") * speed);
    }
}
