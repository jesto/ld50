using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    public Sprite[] sprites;

    public GameObject digit1;
    public GameObject digit2;
    public GameObject digit3;
    public GameObject digit4;

    private Image digit1_image;
    private Image digit2_image;
    private Image digit3_image;
    private Image digit4_image;

    private int time_digit1;
    private int time_digit2;
    private int time_digit3;
    private int time_digit4;

    private GameDirector gamedirector;
    
    // Start is called before the first frame update
    void Start()
    {
        gamedirector = FindObjectOfType<GameDirector>();
        digit1_image = digit1.GetComponent<Image>();
        digit2_image = digit2.GetComponent<Image>();
        digit3_image = digit3.GetComponent<Image>();
        digit4_image = digit4.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        convert_sec2min(); 

        assign_sprites(); 
    }


    void convert_sec2min()
    {
        float timeleft_gd = gamedirector.playerTimeLeft; //fetch timeleft from gamedirector

        int timeleft = (int)(timeleft_gd); //cut off milliseconds from timeleft so we can work with it; now time left in seconds

        int minutes = (int)(timeleft / 60 ); // how many minutes are left

        decimal seconds = timeleft % 60; //how many seconds are left

        time_digit1 = (int)(minutes / 10); //first digit of minutes
                                          
        time_digit2 = (int)(minutes % 10); //second digit of minutes

        time_digit3 = (int)(seconds / 10);//first digit of seconds
        time_digit4 = (int)( seconds % 10);  //second digit of seconds
    }
    
    void assign_sprites()
    {
        digit1_image.sprite = sprites[time_digit1];
        digit2_image.sprite = sprites[time_digit2];     
        digit3_image.sprite = sprites[time_digit3];
        digit4_image.sprite = sprites[time_digit4];
    }
}
