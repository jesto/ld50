using UnityEngine;

public class Player : MonoBehaviour
{
    private GameDirector gameDirector;
    public GameObject meleeAttack1;
    public double attackSpeed = 1d;
    private float lastAttack;
    private Animator animator;

    void Start()
    {
        gameDirector = FindObjectOfType<GameDirector>();
        animator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time - lastAttack > attackSpeed)
        {
            lastAttack = Time.time;
            FireToCursor();
        }
    }
    
    private void FireToCursor()
    {
        var position = gameObject.transform.position;
        var pos = Camera.main!
            .ScreenToWorldPoint(Input.mousePosition);
        
        var rotation = Mathf.Atan2( position.y-pos.y, position.x-pos.x) * Mathf.Rad2Deg;

        var diff = (Vector2) (pos - position);
        diff.Normalize();

        var spawnPos = new Vector3(diff.x, diff.y, 0f);
        spawnPos.Scale(new Vector3(1.25f, 1.25f));

        Instantiate(
            meleeAttack1,
            spawnPos + position, 
            Quaternion.Euler(0, 0, rotation), 
            transform);
    }

    public void Pickup(Collectable collectable)
    {
        gameDirector.AddTime(collectable.timeReward);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("EnemyAttack") && col.IsTouching(GetComponent<Collider2D>()))
        {
            var attack = col.gameObject.GetComponent<Attack>();
            Hit(attack.damage);
        }
    }
    
    public void Hit(int damage_dealt)
    {
        gameDirector.SubstractTime(damage_dealt);
        animator.Play("PlayerTakesDamage");
    }
}
