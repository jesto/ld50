using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttackMovement : MonoBehaviour
{
    public float speed = 1f;
    public Animator animator;
    public Rigidbody2D rb;
    public float timeAlive = 1;
    private static readonly int TimeLeft = Animator.StringToHash("TimeLeft");
    private Vector2 target;

    // Start is called before the first frame update
    void Start()
    {
        var vector2 = new Vector2(transform.position.x, transform.position.y);
        var d = target - vector2;
        d.Normalize();

        rb.velocity = new Vector2(d.x * speed, d.y * speed);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == default)
            Destroy(gameObject);
        
        timeAlive -= Time.deltaTime;
        
        animator.SetFloat(TimeLeft, timeAlive);
    }

    public void EndAnimationDonePlaying()
    {
        Destroy(gameObject);
    }

    public void SetTarget(Vector3 pos)
    {
        target = new Vector2(pos.x, pos.y);
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            timeAlive = -1;
        }
    }
}
