using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameOver : MonoBehaviour
{
    [SerializeField] private GameObject gameOverMenu;
    [SerializeField] private GameObject snoozedText;
    
    private void GameOverAction()
    {
        gameOverMenu.SetActive(true);
        Time.timeScale = 0f;
        snoozedText.GetComponent<TMP_Text>().text = $"YOU SNOOZED FOR {Math.Round(Time.timeSinceLevelLoad/60, 2)} minutes";
    }
    
    private UnityAction onGameOver;

    private void Awake()
    {
        onGameOver = GameOverAction;
    }
    
    private void OnEnable()
    {
        EventManager.StartListening("GameOver", onGameOver);
    }

    private void OnDisable()
    {
        EventManager.StopListening("GameOver", onGameOver);
    }
}
