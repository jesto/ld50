using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    [SerializeField]
    public float playerTimeLeft = 60f;
    
    [SerializeField]
    public int snoozeTime = 30;

    [SerializeField]
    public int noOfSnoozes = 3;

    void Start()
    {
        Time.timeScale = 1f;
    }

    void FixedUpdate()
    {
        playerTimeLeft -= Time.fixedDeltaTime;
        EnsurePlayerTimeNotBelowZero();

        if (playerTimeLeft <= 0)
        {
            EventManager.TriggerEvent(noOfSnoozes > 0 ? "Alarm" : "GameOver");
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            EventManager.TriggerEvent("OpenMenu");
        }
    }

    public void AddTime(int time)
    {
        playerTimeLeft += time;
    }

    public void SubstractTime(int time)
    {
        playerTimeLeft -= time;
        EnsurePlayerTimeNotBelowZero();
    }

    private void EnsurePlayerTimeNotBelowZero()
    {
        if (playerTimeLeft < 0)
            playerTimeLeft = 0;
    }

    private void Snoozed()
    {
        AddTime(snoozeTime);
        noOfSnoozes--;
    }
    
    private UnityAction onOpenMenu;
    private UnityAction onSnoozed;

    private void Awake()
    {
        onOpenMenu = () => SceneManager.LoadScene("Menu");
        onSnoozed = Snoozed;
    }

    private void OnEnable()
    {
        EventManager.StartListening("OpenMenu", onOpenMenu);
        EventManager.StartListening("Snoozed", onSnoozed);
    }

    private void OnDisable()
    {
        EventManager.StopListening("OpenMenu", onOpenMenu);
        EventManager.StopListening("Snoozed", onSnoozed);
    }
}
