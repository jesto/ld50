using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    private IList<Collectable> attracting = new Collection<Collectable>();
    public float strength = 1.0f;
    private CircleCollider2D circleCollider;

    private void Start()
    {
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        var collectable = col.GetComponent<Collectable>();

        if (collectable is not null)
            attracting.Add(collectable);
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        var collectable = col.GetComponent<Collectable>();

        if (collectable is not null)
        {
            collectable.Velocity(Vector2.zero);

            attracting.Remove(collectable);
        }
    }
    
    private void Update()
    {
        var position = transform.position;
        
        foreach (var collectable in new List<Collectable>(attracting))
        {
            var collectablePosition = collectable.transform.position;

            var pos = Vector3.Lerp(
                collectablePosition, 
                position, 
                Math.Clamp(circleCollider.radius - Vector3.Distance(collectablePosition, position), 0f, 1f));
            
            collectable.Velocity((pos - collectablePosition) * strength);
        }
    }
}
