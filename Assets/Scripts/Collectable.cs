using UnityEngine;

public class Collectable : MonoBehaviour
{
    private Rigidbody2D rb;
    public int timeReward = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Velocity(Vector2 pos)
    {
        rb.velocity = pos;
    }
}
