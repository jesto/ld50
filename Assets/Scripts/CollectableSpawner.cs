using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollectableSpawner : MonoBehaviour
{
    [SerializeField] private GameObject collectable;

    private UnityAction<Transform> onSpawnCollectable;

    private void SpawnCollectable(Transform spawnPosition)
    {
        var spawnedCollectable = Instantiate(collectable, spawnPosition.position, spawnPosition.rotation, gameObject.transform);
    }

    private void Awake()
    {
        onSpawnCollectable = (spawnPosition) => SpawnCollectable(spawnPosition);
    }

    private void OnEnable()
    {
        EventManager.StartListening("SpawnCollectable", onSpawnCollectable);
    }

    private void OnDisable()
    {
        EventManager.StopListening("SpawnCollectable", onSpawnCollectable);
    }
}
