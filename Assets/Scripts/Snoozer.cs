using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Events;

public class Snoozer : MonoBehaviour
{
    [SerializeField] private GameObject alarmMenu;

    public void Snooze()
    {
        alarmMenu.SetActive(false);
        Time.timeScale = 1f;
        EventManager.TriggerEvent("Snoozed");
    }
    
    private void Alarm()
    {
        alarmMenu.SetActive(true);
        Time.timeScale = 0f;
    }
    
    private UnityAction onAlarm;

    private void Awake()
    {
        onAlarm = Alarm;
    }
    
    private void OnEnable()
    {
        EventManager.StartListening("Alarm", onAlarm);
    }

    private void OnDisable()
    {
        EventManager.StopListening("Alarm", onAlarm);
    }
}
