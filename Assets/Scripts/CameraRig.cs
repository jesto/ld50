using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour
{
    public GameObject player;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void  Update()
    {
        cam_follow_play();
    }

    void cam_follow_play()
    {
        //Debug.Log(player.transform.position);
        
        var player_pos = player.transform.position;
        var cam_pos = gameObject.transform.position;

        gameObject.transform.position = new Vector3(player_pos.x, player_pos.y, cam_pos.z);
    }
}
