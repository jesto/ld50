using System;
using Pathfinding;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private int health;
    [SerializeField] private int triggerRange = 10;
    [SerializeField] private int fireRange = 5;
    [SerializeField] private GameObject rangedAttack;

    public float attackRate = 1;

    private float lastAttack = 0;
    private bool hybernating = true;
    private AIPath aiPath;
    private Transform healthBar;
    private float initialHealth;

    // Start is called before the first frame update
    void Start()
    {
        aiPath = GetComponent<AIPath>();
        healthBar = transform.Find("Healthbar");
        initialHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        var dist = Vector3.Distance(player.transform.position, transform.position);
        
        if (hybernating && dist < triggerRange)
        {
            hybernating = false;
            aiPath.enabled = true;
        }

        if (hybernating == false && dist < fireRange)
        {
            hybernating = true;
            aiPath.enabled = false;
        }

        ChangeSpriteDirection();

        if (dist < fireRange)
            FireAtPlayer();
    }

    private void FireAtPlayer()
    {
        if (Time.time - lastAttack < attackRate)
            return;
        
        lastAttack = Time.time;
        var position = gameObject.transform.position;
        var pos = player.transform.position;
        
        var rotation = Mathf.Atan2( position.y-pos.y, position.x-pos.x) * Mathf.Rad2Deg;


        var diff = (Vector2) (pos - position);
        diff.Normalize();

        var spawnPos = new Vector3(diff.x, diff.y, 0f);

        var go = Instantiate(
            rangedAttack,
            spawnPos + position, 
            Quaternion.Euler(0, 0, rotation));

        var movement = go.GetComponent<RangedAttackMovement>();
        movement.SetTarget(pos);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Attack"))
        {
            var attack = col.gameObject.GetComponent<Attack>();

            health -= attack.damage;

            if (health <= 0)
                Kill();
            else
                TakeDamage();
        }
    }

    private void Kill()
    {
        EventManager.TriggerEvent("SpawnCollectable", gameObject.transform);

        // death animation
        Destroy(gameObject);
    }

    private void TakeDamage()
    {
        float newXScale = health / initialHealth;
        var healthBarLocalScale = healthBar.localScale;
        healthBar.transform.localScale = new Vector3(newXScale, healthBarLocalScale.y, healthBarLocalScale.z);
    }

    private void ChangeSpriteDirection()
    {
        var diff = player.transform.position - transform.position;
        
        if (diff.x > 0 && Math.Abs(transform.rotation.y - 180f) > 0.0001f)
        {
            transform.rotation = new Quaternion(0f,180f, 0f, 0f);
        }
        else if (diff.x < 0 && transform.rotation.y != 0f)
        {
            transform.rotation = new Quaternion(0f,0f, 0f, 0f);
        }
    }
}
