using UnityEngine;

public class RemoveOnAnimationEnd : MonoBehaviour
{
    public void Remove()
    {
        if (transform.parent is not null)
            Destroy(transform.parent.gameObject);
        else
            Destroy(gameObject);
    }
}
