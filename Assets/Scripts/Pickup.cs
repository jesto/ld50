using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
	private void OnCollisionEnter2D(Collision2D col)
	{
		var player = col.otherCollider.GetComponent<Player>();
		var collectable = col.gameObject.GetComponent<Collectable>();

		if (player is not null && collectable is not null)
		{
			player.Pickup(collectable);
			GameObject.Destroy(collectable.gameObject);
		}
	}
}
